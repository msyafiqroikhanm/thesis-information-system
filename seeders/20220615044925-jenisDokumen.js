"use strict";
const fs = require("fs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(
      fs.readFileSync("./seeders/data/jenisDokumen.json")
    );

    const jenisDokumen = data.map((element) => {
      return {
        nama: element.nama,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });

    await queryInterface.bulkInsert("JenisDokumens", jenisDokumen);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("JenisDokumens", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
