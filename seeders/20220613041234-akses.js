"use strict";
const fs = require("fs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync("./seeders/data/akses.json"));
    const akses = data.map((element) => {
      return {
        nama: element.nama,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("Akses", akses);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Akses", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
