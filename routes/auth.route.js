const router = require("express").Router();
const { check } = require("express-validator");
const AuthController = require("../controllers/auth.controller");
const ValidationMiddleware = require("../middlewares/validation.middleware");

router.post(
  "/login",
  [
    check("email", "Email tidak boleh kosong").notEmpty(),
    check("password", "Password tidak boleh kosong").notEmpty(),
  ],
  ValidationMiddleware.result,
  AuthController.login
);

router.post(
  "/logout",
  [check("email", "Email tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  AuthController.logout
);

router.post(
  "/forgot-password",
  [check("email", "Email tidak boleh kosong").isEmail()],
  ValidationMiddleware.result,
  AuthController.forgotPassword
);

router.post(
  "/verify-token",
  [
    check("email", "Email tidak boleh kosong").isEmail(),
    check("token", "Token tidak boleh kosong").notEmpty(),
  ],
  ValidationMiddleware.result,
  AuthController.verifyToken
);

router.post(
  "/change-password",
  [
    check("password", "Password tidak boleh kosong").notEmpty(),
    check(
      "password_confirmation",
      "Password_confirmation tidak boleh kosong"
    ).notEmpty(),
    check("email", "Email tidak boleh kosong").isEmail(),
  ],
  ValidationMiddleware.result,
  AuthController.changePassword
);

module.exports = router;
