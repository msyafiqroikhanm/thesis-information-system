const SidangControlller = require("../controllers/sidang.controller");
const router = require("express").Router();
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const { check } = require("express-validator");
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.admin,
    ]);
  },
  upload.single("jadwal_sidang"),
  [
    check(
      "pengajuanSeminarSidangId",
      "Atribut pengajuanSeminarSidangId tidak boleh kosong"
    ).isNumeric(),
    check(
      "waktu",
      "Atribut waktu tidak boleh kosong dan harus mengikuti format: yyyy-mm-dd"
    )
      .isISO8601()
      .toDate(),
    check("tempat", "Atribut tempat tidak boleh kosong").notEmpty(),
    check(
      "k_penguji",
      "Atribut k_penguji tidak boleh kosong dan harus berupa integer"
    ).isNumeric(),
    check(
      "penguji1",
      "Atribut penguji1 tidak boleh kosong dan harus berupa integer"
    ).isNumeric(),
    check(
      "penguji2",
      "Atribut penguji2 tidak boleh kosong dan harus berupa integer"
    ).isNumeric(),
  ],
  ValidationMiddleware.result,
  SidangControlller.createJadwal
);

router.put(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  upload.single("jadwal_sidang"),
  SidangControlller.updateJadwal
);

router.delete(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  SidangControlller.deleteJadwal
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.mahasiswa,
    ]);
  },
  SidangControlller.listJadwal
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.mahasiswa,
    ]);
  },
  SidangControlller.detailJadwal
);

router.post(
  "/:uuid/berita-acara",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen]);
  },
  upload.single("berita_acara_sidang"),
  ValidationMiddleware.file,
  SidangControlller.uploadBeritaAcara
);

router.post(
  "/:uuid/perbaikan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("perbaikan_skripsi"),
  ValidationMiddleware.file,
  SidangControlller.uploadPerbaikan
);

router.get(
  "/:uuid/perbaikan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.dosen,
      akses.koorprodi,
      akses.mahasiswa,
    ]);
  },
  SidangControlller.detailPerbaikan
);

router.post(
  "/:uuid/penilaian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen]);
  },
  upload.single("penilaian_ujian"),
  ValidationMiddleware.file,
  [check("nilai", "Atribut Nilai tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  SidangControlller.uploadPenilaian
);

router.get(
  "/:uuid/penilaian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.dosen,
    ]);
  },
  SidangControlller.detailPenilaian
);

module.exports = router;
