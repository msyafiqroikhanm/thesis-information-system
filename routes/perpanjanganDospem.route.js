const router = require("express").Router();
const DospemController = require("../controllers/dospem.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("perpanjangan_dospem"),
  ValidationMiddleware.file,
  DospemController.pengajuanPerpanjangan
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  DospemController.daftarPengajuanPerpanjangan
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  DospemController.detailPengajuanPerpanjangan
);

router.put(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  upload.single("perpanjangan_surat_tugas_dospem"),
  DospemController.penetapanPerpanjangan
);

module.exports = router;
