const router = require("express").Router();
const DospemController = require("../controllers/dospem.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("pengajuan_dospem"),
  [
    check("judul", "Judul tidak boleh kosong").notEmpty(),
    check("deskripsi", "Deskripsi tidak boleh kosong").notEmpty(),
    check("pembimbing1", "Pembimbing1 harus berupa integer")
      .isNumeric()
      .optional({ nullable: true }),
    check("pembimbing2", "Pembimbing2 harus berupa integer")
      .isNumeric()
      .optional({ nullable: true }),
  ],
  ValidationMiddleware.result,
  DospemController.pengajuan
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  DospemController.daftarPengajuan
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.mahasiswa,
    ]);
  },
  DospemController.detailPengajuan
);

router.put(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  upload.single("surat_tugas_dospem"),
  [
    check("uuid", "UUID tidak boleh kosong").notEmpty(),
    check("pembimbing1", "Pembimbing1 harus berupa integer")
      .isNumeric()
      .optional({ nullable: true }),
    check("pembimbing2", "Pembimbing2 harus berupa integer")
      .isNumeric()
      .optional({ nullable: true }),
  ],
  ValidationMiddleware.result,
  DospemController.penetapan
);

module.exports = router;
