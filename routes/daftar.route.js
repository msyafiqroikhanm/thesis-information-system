const DaftarController = require("../controllers/daftar.controller");
const router = require("express").Router();
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };

router.get(
  "/bimbingan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.dosen,
    ]);
  },
  DaftarController.mahasiswaBimbingan
);

router.get(
  "/dosen-bimbingan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.koorprodi]);
  },
  DaftarController.dosenBimbingan
);

router.get(
  "/skripsi-bimbingan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.dosen,
    ]);
  },
  DaftarController.skripsiBimbingan
);

router.get(
  "/mahasiswa-skripsi",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.admin,
    ]);
  },
  DaftarController.mahasiswaSkripsi
);

module.exports = router;
