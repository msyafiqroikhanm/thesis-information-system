const router = require("express").Router();
const ArsipController = require("../controllers/arsip.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };

router.get(
  "/skripsi",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.dosen,
      akses.admin,
      akses.mahasiswa,
    ]);
  },
  ArsipController.arsipSkripsi
);

router.get(
  "/penilaian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipPenilaian
);

router.get(
  "/surat-tugas",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipSuratTugas
);

router.get(
  "/perpanjangan-surat-tugas",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipPerpanjanganSuratTugas
);

router.get(
  "/berita-acara",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipBeritaAcara
);

router.get(
  "/jadwal",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipJadwal
);

router.get(
  "/surat-izin-penelitian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.admin]);
  },
  ArsipController.arsipSuratIzinPenelitian
);

module.exports = router;
