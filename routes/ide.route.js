const router = require("express").Router();
const IdeController = require("../controllers/ide.controller");
const AuthMidddleware = require("../middlewares/auth.middlewares");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const { check } = require("express-validator");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.dosen]);
  },
  [check("judul", "Atribut judul tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  IdeController.createIde
);

router.get(
  "/",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [
      akses.dosen,
      akses.koorprodi,
      akses.admin,
      akses.mahasiswa,
    ]);
  },
  IdeController.getAllIde
);

router.get(
  "/:uuid",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [
      akses.dosen,
      akses.koorprodi,
      akses.admin,
      akses.mahasiswa,
    ]);
  },
  IdeController.getDetailIde
);

router.delete(
  "/:uuid",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.dosen]);
  },
  IdeController.deleteIde
);

router.post(
  "/:uuid/penelitian",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.fields([{ name: "biodata", maxCount: 1 }]),
  ValidationMiddleware.files,
  IdeController.createPengajuanPenelitian
);

router.get(
  "/:uuid/penelitian",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.dosen]);
  },
  IdeController.getPengajuanPenelitian
);

router.put(
  "/:uuid_ide/penelitian/:uuid_penelitian/setuju",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.dosen]);
  },
  IdeController.acceptPengajuanPenelitian
);

router.put(
  "/:uuid_ide/penelitian/:uuid_penelitian/tolak",
  AuthMidddleware.authentication,
  (req, res, next) => {
    AuthMidddleware.authorization(req, res, next, [akses.dosen]);
  },
  [check("keterangan", "Atribut keterangan tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  IdeController.rejectPengajuanPenelitian
);

module.exports = router;
