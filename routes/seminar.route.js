const router = require("express").Router();
const SeminarController = require("../controllers/seminar.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  upload.single("jadwal_seminar"),
  [
    check(
      "pengajuanSeminarSidangId",
      "Atribut pengajuanSeminarSidangId harus berupa integer"
    ).isNumeric(),
    check("penguji1", "Atribut penguji1 harus berupa integer").isNumeric(),
    check("penguji2", "Atribut penguji2 harus berupa integer").isNumeric(),
    check("waktu", "Atribut waktu tidak boleh kosong").notEmpty(),
    check("waktu", "Atribut waktu harus mengikuti format: yyyy-mm-dd")
      .isISO8601()
      .toDate(),
  ],
  ValidationMiddleware.result,
  SeminarController.add
);

router.put(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  upload.single("jadwal_seminar"),
  SeminarController.update
);

router.delete(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  SeminarController.delete
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  SeminarController.getAll
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  SeminarController.getDetail
);

router.post(
  "/:uuid/berita-acara",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.dosen,
    ]);
  },
  upload.single("berita_acara_sempro"),
  ValidationMiddleware.file,
  SeminarController.uploadBeritaAcara
);

router.post(
  "/:uuid/perbaikan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("revisi_proposal"),
  ValidationMiddleware.file,
  SeminarController.uploadPerbaikan
);

router.get(
  "/:uuid/perbaikan",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.dosen,
      akses.mahasiswa,
    ]);
  },
  SeminarController.getPerbaikan
);

router.post(
  "/:uuid/penilaian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen]);
  },
  upload.single("penilaian_sempro"),
  ValidationMiddleware.file,
  [check("nilai", "Atribut nilai tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  SeminarController.uploadPenilaian
);

router.get(
  "/:uuid/penilaian",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen, akses.admin]);
  },
  SeminarController.getPenilaian
);

module.exports = router;
