const {
  AksesPengguna,
  Pengguna,
  BiodataPengguna,
  Status,
} = require("../models");

const createAksesPengguna = async (formAkses) => {
  return await AksesPengguna.create(formAkses);
};

const updateStatus = async (statusId, penggunaId) => {
  return await AksesPengguna.update({ statusId }, { where: { penggunaId } });
};

const daftarStatusMahasiswa = async () => {
  return await AksesPengguna.findAll({
    where: { aksesId: 4, deletedAt: null },
    attributes: ["id"],
    include: [
      {
        model: Pengguna,
        as: "Pengguna",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["id", "nama", "nim", "angkatan"],
        },
      },
      {
        model: Status,
        as: "Status",
        attributes: ["id", "nama"],
      },
    ],
  });
};

module.exports = { createAksesPengguna, updateStatus, daftarStatusMahasiswa };
