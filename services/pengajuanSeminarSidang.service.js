const {
  PengajuanSeminarSidang,
  SuratTugasDospem,
  Pengguna,
  BiodataPengguna,
  Dokumen,
  JenisDokumen,
} = require("../models");
const { createDok } = require("./dokumen.service");
const jenisDokumen = {
  naskahSkripsi: 21,
  biodata: 12,
  suratPermohonanUjian: 13,
  suratPersetujuanDospem: 14,
  praTranskip: 15,
  photo: 16,
  ktm: 17,
  bimbingan: 18,
  pkl: 19,
  pkm: 20,
  suratPernyataanDospem: 22,
};
const { v4: uuidv4 } = require("uuid");
const { Op } = require("sequelize");
const pathDocument = process.env.BASE_URL + "documents/";

const createPengajuanSidang = async (formPengajuan, dokumen) => {
  const pengajuanSidang = await PengajuanSeminarSidang.create(formPengajuan);
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.naskahSkripsi,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.naskah_skripsi[0].filename,
    filesize: dokumen.naskah_skripsi[0].size,
    mimetype: dokumen.naskah_skripsi[0].mimetype,
    path: pathDocument + dokumen.naskah_skripsi[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.biodata,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.biodata[0].filename,
    filesize: dokumen.biodata[0].size,
    mimetype: dokumen.biodata[0].mimetype,
    path: pathDocument + dokumen.biodata[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.suratPermohonanUjian,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.surat_permohonan_ujian_skripsi[0].filename,
    filesize: dokumen.surat_permohonan_ujian_skripsi[0].size,
    mimetype: dokumen.surat_permohonan_ujian_skripsi[0].mimetype,
    path: pathDocument + dokumen.surat_permohonan_ujian_skripsi[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.suratPersetujuanDospem,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.lembar_persetujuan_dospem[0].filename,
    filesize: dokumen.lembar_persetujuan_dospem[0].size,
    mimetype: dokumen.lembar_persetujuan_dospem[0].mimetype,
    path: pathDocument + dokumen.lembar_persetujuan_dospem[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.praTranskip,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.pra_transkrip[0].filename,
    filesize: dokumen.pra_transkrip[0].size,
    mimetype: dokumen.pra_transkrip[0].mimetype,
    path: pathDocument + dokumen.pra_transkrip[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.photo,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.photo[0].filename,
    filesize: dokumen.photo[0].size,
    mimetype: dokumen.photo[0].mimetype,
    path: pathDocument + dokumen.photo[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.ktm,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.scan_ktm[0].filename,
    filesize: dokumen.scan_ktm[0].size,
    mimetype: dokumen.scan_ktm[0].mimetype,
    path: pathDocument + dokumen.scan_ktm[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.bimbingan,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.surat_tugas_dan_lembar_bimbingan[0].filename,
    filesize: dokumen.surat_tugas_dan_lembar_bimbingan[0].size,
    mimetype: dokumen.surat_tugas_dan_lembar_bimbingan[0].mimetype,
    path: pathDocument + dokumen.surat_tugas_dan_lembar_bimbingan[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.pkl,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.bukti_pkl[0].filename,
    filesize: dokumen.bukti_pkl[0].size,
    mimetype: dokumen.bukti_pkl[0].mimetype,
    path: pathDocument + dokumen.bukti_pkl[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.pkm,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.bukti_pkm[0].filename,
    filesize: dokumen.bukti_pkm[0].size,
    mimetype: dokumen.bukti_pkm[0].mimetype,
    path: pathDocument + dokumen.bukti_pkm[0].filename,
  });
  await createDok({
    uuid: uuidv4(),
    jenisDokumenId: jenisDokumen.suratPernyataanDospem,
    pengajuanSeminarSidangId: pengajuanSidang.id,
    filename: dokumen.surat_pernyataan_dospem[0].filename,
    filesize: dokumen.surat_pernyataan_dospem[0].size,
    mimetype: dokumen.surat_pernyataan_dospem[0].mimetype,
    path: pathDocument + dokumen.surat_pernyataan_dospem[0].filename,
  });

  return pengajuanSidang;
};

const createPengajuanSeminar = async (
  formSeminar,
  formProposal,
  formBimbingan,
  formPernyataan
) => {
  const pengajuanSeminar = await PengajuanSeminarSidang.create(formSeminar);

  formProposal.pengajuanSeminarSidangId = pengajuanSeminar.id;
  formBimbingan.pengajuanSeminarSidangId = pengajuanSeminar.id;
  formPernyataan.pengajuanSeminarSidangId = pengajuanSeminar.id;

  await createDok(formProposal);
  await createDok(formBimbingan);
  await createDok(formPernyataan);

  return pengajuanSeminar;
};

const getAllPengajuanSidang = async () => {
  return PengajuanSeminarSidang.findAll({
    where: {
      persetujuan: null,
      sidang: true,
      seminar: false,
    },
    include: {
      model: SuratTugasDospem,
      as: "SuratTugasDospem",
      include: [
        {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nim", "angkatan"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing1",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing2",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
      ],
    },
  });
};

const getAllPengajuanSeminar = async () => {
  return PengajuanSeminarSidang.findAll({
    where: {
      persetujuan: null,
      seminar: true,
    },
    include: {
      model: SuratTugasDospem,
      as: "SuratTugasDospem",
      include: [
        {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nim", "angkatan"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing1",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing2",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
      ],
    },
  });
};

const getDetailPengajuanSidang = async (where) => {
  return await PengajuanSeminarSidang.findOne({
    where,
    include: [
      {
        model: SuratTugasDospem,
        as: "SuratTugasDospem",
        include: [
          {
            model: Pengguna,
            as: "Mahasiswa",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nim", "angkatan"],
            },
          },
          {
            model: Pengguna,
            as: "Pembimbing1",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nidn", "nip"],
            },
          },
          {
            model: Pengguna,
            as: "Pembimbing2",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nidn", "nip"],
            },
          },
          {
            model: Dokumen,
            as: "Dokumen",
            attributes: ["id", "uuid", "path"],
            include: {
              model: JenisDokumen,
              as: "JenisDokumen",
              attributes: ["nama"],
            },
          },
        ],
      },
      {
        model: Dokumen,
        as: "DokumenPengajuanSeminarSidang",
        attributes: ["id", "uuid", "path"],
        include: {
          model: JenisDokumen,
          as: "JenisDokumen",
          attributes: ["nama"],
        },
      },
    ],
  });
};

const getDetailPengajuanSeminar = async (where) => {
  return await PengajuanSeminarSidang.findOne({
    where,
    include: {
      model: SuratTugasDospem,
      as: "SuratTugasDospem",
      include: [
        {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nim", "angkatan"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing1",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing2",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nidn", "nip"],
          },
        },
        {
          model: Dokumen,
          as: "Dokumen",
          attributes: ["id", "uuid", "path"],
          where: {
            jenisDokumenId: { [Op.in]: [2, 5, 6, 7] },
          },
          include: {
            model: JenisDokumen,
            as: "JenisDokumen",
            attributes: ["nama"],
          },
        },
      ],
    },
  });
};

const updatePengajuanSeminarSidang = async (form, where) => {
  await PengajuanSeminarSidang.update(form, { where });
};

module.exports = {
  createPengajuanSeminar,
  getAllPengajuanSeminar,
  getDetailPengajuanSeminar,
  getDetailPengajuanSidang,
  updatePengajuanSeminarSidang,
  createPengajuanSidang,
  getAllPengajuanSidang,
};
