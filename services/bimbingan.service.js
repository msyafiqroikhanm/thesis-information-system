const { Bimbingan, SuratTugasDospem } = require("../models");

const createBimbingan = async (form) => {
  return await Bimbingan.create(form);
};

const getBimbinganBy = async (where) => {
  return await Bimbingan.findOne({
    where,
    include: { model: SuratTugasDospem, as: "SuratTugasDospem" },
  });
};

const getAllBimbingan = async () => {
  return await Bimbingan.findAll({
    where: { deletedAt: null },
    include: { model: SuratTugasDospem, as: "SuratTugasDospem" },
  });
};

const softDeleteBimbingan = async (where) => {
  return await Bimbingan.update({ deletedAt: new Date() }, { where });
};

module.exports = {
  createBimbingan,
  getBimbinganBy,
  softDeleteBimbingan,
  getAllBimbingan,
};
