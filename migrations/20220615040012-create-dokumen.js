"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Dokumens", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        type: Sequelize.UUID,
      },
      jenisDokumenId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
        references: { model: "JenisDokumens", key: "id" },
      },
      pengajuanPenelitianDosenId: {
        type: Sequelize.INTEGER(5),
      },
      suratTugasDospemId: {
        type: Sequelize.INTEGER(5),
        references: { model: "SuratTugasDospems", key: "id" },
      },
      bimbinganId: {
        type: Sequelize.INTEGER(5),
      },
      pengajuanSeminarSidangId: {
        type: Sequelize.INTEGER(5),
      },
      seminarSidangId: {
        type: Sequelize.INTEGER(5),
      },
      filename: {
        type: Sequelize.STRING,
      },
      filesize: {
        type: Sequelize.INTEGER(10),
      },
      mimetype: {
        type: Sequelize.STRING,
      },
      path: {
        type: Sequelize.STRING,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Dokumens");
  },
};
