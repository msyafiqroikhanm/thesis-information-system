"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("PengajuanPenelitianIdeDosens", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      idePenelitianId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      mahasiswaId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      penerimaan: {
        type: Sequelize.BOOLEAN,
      },
      keterangan: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("PengajuanPenelitianIdeDosens");
  },
};
