"use strict";

const { DataTypes } = require("sequelize");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("SuratTugasDospems", "nilaiSeminar", {
      type: DataTypes.FLOAT,
      after: "keterangan",
    });
    await queryInterface.addColumn("SuratTugasDospems", "nilaiSidang", {
      type: DataTypes.FLOAT,
      after: "nilaiSeminar",
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn(
      "SuratTugasDospems",
      "nilaiSeminar",
      null
    );
    await queryInterface.removeColumn("SuratTugasDospems", "nilaiSidang", null);
  },
};
