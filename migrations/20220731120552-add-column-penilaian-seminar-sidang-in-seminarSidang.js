"use strict";

const { DataTypes } = require("sequelize");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("SeminarSidangs", "nilaiDospem1", {
      type: DataTypes.FLOAT,
    });
    await queryInterface.addColumn("SeminarSidangs", "nilaiDospem2", {
      type: DataTypes.FLOAT,
    });
    await queryInterface.addColumn("SeminarSidangs", "nilaiKPenguji", {
      type: DataTypes.FLOAT,
    });
    await queryInterface.addColumn("SeminarSidangs", "nilaiPenguji1", {
      type: DataTypes.FLOAT,
    });
    await queryInterface.addColumn("SeminarSidangs", "nilaiPenguji2", {
      type: DataTypes.FLOAT,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn("SeminarSidangs", "nilaiDospem1", null);
    await queryInterface.removeColumn("SeminarSidangs", "nilaiDospem2", null);
    await queryInterface.removeColumn("SeminarSidangs", "nilaiKPenguji", null);
    await queryInterface.removeColumn("SeminarSidangs", "nilaiPenguji1", null);
    await queryInterface.removeColumn("SeminarSidangs", "nilaiPenguji2", null);
  },
};
