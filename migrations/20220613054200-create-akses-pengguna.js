"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("AksesPenggunas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      penggunaId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
        references: { model: "Penggunas", key: "id" },
      },
      statusId: {
        type: Sequelize.INTEGER(5),
        references: { model: "Statuses", key: "id" },
      },
      aksesId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
        references: { model: "Akses", key: "id" },
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("AksesPenggunas");
  },
};
