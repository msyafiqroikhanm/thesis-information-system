"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("PengajuanSeminarSidangs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      suratTugasDospemId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      seminarSidangId: {
        type: Sequelize.INTEGER(5),
      },
      seminar: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      sidang: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      persetujuan: {
        type: Sequelize.BOOLEAN,
      },
      keterangan: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("PengajuanSeminarSidangs");
  },
};
