const { validationResult } = require("express-validator");

class ValidationMiddleware {
  static async result(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        next({
          code: 400,
          status: "Bad Request",
          message: errors.errors,
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }

  static async file(req, res, next) {
    try {
      if (!req.file) {
        next({
          code: 400,
          status: "Bad Request",
          message: "File lampiran harus wajib diisi",
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }

  static async files(req, res, next) {
    try {
      if (!req.files) {
        next({
          code: 400,
          status: "Bad Request",
          message: "File lampiran harus wajib diisi",
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }

  static async pengajuanSidang(req, res, next) {
    try {
      let errors = [];
      if (!req.files.naskah_skripsi) {
        errors.push("Dokumen naskah_skripsi tidak boleh kosong");
      }

      if (!req.files.biodata) {
        errors.push("Dokumen biodata tidak boleh kosong");
      }

      if (!req.files.surat_permohonan_ujian_skripsi) {
        errors.push(
          "Dokumen surat_permohonan_ujian_skripsi tidak boleh kosong"
        );
      }

      if (!req.files.lembar_persetujuan_dospem) {
        errors.push("Dokumen lembar_persetujuan_dospem tidak boleh kosong");
      }

      if (!req.files.pra_transkrip) {
        errors.push("Dokumen pra_transkrip tidak boleh kosong");
      }

      if (!req.files.photo) {
        errors.push("Dokumen photo tidak boleh kosong");
      }

      if (!req.files.scan_ktm) {
        errors.push("Dokumen scan_ktm tidak boleh kosong");
      }

      if (!req.files.surat_tugas_dan_lembar_bimbingan) {
        errors.push(
          "Dokumen surat_tugas_dan_lembar_bimbingan tidak boleh kosong"
        );
      }

      if (!req.files.bukti_pkl) {
        errors.push("Dokumen bukti_pkl tidak boleh kosong");
      }

      if (!req.files.bukti_pkm) {
        errors.push("Dokumen bukti_pkm tidak boleh kosong");
      }

      if (!req.files.surat_pernyataan_dospem) {
        errors.push("Dokumen surat_pernyataan_dospem tidak boleh kosong");
      }

      if (errors.length > 0) {
        next({
          code: 400,
          status: "Bad Request",
          message: errors,
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ValidationMiddleware;
