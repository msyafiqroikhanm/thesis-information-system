"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Pengguna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pengguna.belongsTo(models.BiodataPengguna, {
        foreignKey: "biodataId",
        as: "Biodata",
      });
      Pengguna.hasMany(models.AksesPengguna, {
        foreignKey: "penggunaId",
        as: "AksesPengguna",
      });

      Pengguna.hasMany(models.SuratTugasDospem, {
        foreignKey: "mahasiswaId",
        as: "SuratTugasPenelitian",
      });

      Pengguna.hasMany(models.SuratTugasDospem, {
        foreignKey: "pembimbing1",
        as: "SuratTugasPembimbing1",
      });

      Pengguna.hasMany(models.SuratTugasDospem, {
        foreignKey: "pembimbing2",
        as: "SuratTugasPembimbing2",
      });

      Pengguna.hasMany(models.SeminarSidang, {
        foreignKey: "k_penguji",
        as: "KetuaSeminarSidang",
      });

      Pengguna.hasMany(models.SeminarSidang, {
        foreignKey: "penguji1",
        as: "Penguji1SeminarSidang",
      });

      Pengguna.hasMany(models.SeminarSidang, {
        foreignKey: "penguji2",
        as: "Penguji2SeminarSidang",
      });

      Pengguna.hasMany(models.IdePenelitian, {
        foreignKey: "dosenId",
        as: "IdePenelitian",
      });

      Pengguna.hasMany(models.PengajuanPenelitianIdeDosen, {
        foreignKey: "mahasiswaId",
        as: "PengajuanPenelitian",
      });
    }
  }
  Pengguna.init(
    {
      biodataId: DataTypes.INTEGER,
      uuid: DataTypes.UUID,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      active: DataTypes.BOOLEAN,
      access_token: DataTypes.STRING,
      forgot_pass_token: DataTypes.STRING,
      verifiedAt: DataTypes.DATE,
      forgot_pass_token_expiredAt: DataTypes.DATE,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Pengguna",
    }
  );
  return Pengguna;
};
