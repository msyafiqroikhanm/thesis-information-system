"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class AksesPengguna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      AksesPengguna.belongsTo(models.Pengguna, {
        foreignKey: "penggunaId",
        as: "Pengguna",
      });

      AksesPengguna.belongsTo(models.Status, {
        foreignKey: "statusId",
        as: "Status",
      });
    }
  }
  AksesPengguna.init(
    {
      penggunaId: DataTypes.INTEGER,
      statusId: DataTypes.INTEGER,
      aksesId: DataTypes.INTEGER,
      aksesId: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "AksesPengguna",
    }
  );
  return AksesPengguna;
};
