"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class BiodataPengguna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BiodataPengguna.hasOne(models.Pengguna, {
        foreignKey: "biodataId",
        as: "Biodata",
      });

      BiodataPengguna.hasMany(models.Minat, {
        foreignKey: "biodataId",
        as: "Minat",
      });
    }
  }
  BiodataPengguna.init(
    {
      nama: DataTypes.STRING,
      nip: DataTypes.STRING,
      nidn: DataTypes.STRING,
      nim: DataTypes.STRING,
      angkatan: DataTypes.CHAR,
      no_hp: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "BiodataPengguna",
    }
  );
  return BiodataPengguna;
};
