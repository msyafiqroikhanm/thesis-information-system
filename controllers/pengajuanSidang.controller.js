const { getDetailSuratTugas } = require("../services/suratTugasDospem.service");
const {
  createPengajuanSidang,
} = require("../services/pengajuanSeminarSidang.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const { v4: uuidv4 } = require("uuid");
const {
  getAllPengajuanSidang,
  getDetailPengajuanSidang,
} = require("../services/pengajuanSeminarSidang.service");

class PengajuanSidangController {
  static async pengajuanSidang(req, res, next) {
    try {
      const suratTugasDospem = await getDetailSuratTugas({
        mahasiswaId: req.user.id,
        persetujuan: true,
      });
      if (!suratTugasDospem) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Surat Tugas Dospem tidak ditemukan",
        };
      }

      const sidang = await createPengajuanSidang(
        {
          uuid: uuidv4(),
          suratTugasDospemId: suratTugasDospem.id,
          seminar: false,
          sidang: true,
        },
        req.files
      );

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil melakukan pengajuan ujian skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async daftarPengajuanSidang(req, res, next) {
    try {
      const daftarPengajuanSidang = await getAllPengajuanSidang();
      if (daftarPengajuanSidang === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar Pengajuan Sidang Kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar pengajuan sidang ditemukan",
        daftarPengajuanSidang
      );
    } catch (error) {
      next(error);
    }
  }
  static async detailPengajuanSidang(req, res, next) {
    try {
      const detailPengajuanSidang = await getDetailPengajuanSidang({
        uuid: req.params.uuid,
        sidang: true,
        seminar: false,
      });
      if (!detailPengajuanSidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data Pengajuan Sidang tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data pengajuan sidang ditemukan",
        detailPengajuanSidang
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = PengajuanSidangController;
