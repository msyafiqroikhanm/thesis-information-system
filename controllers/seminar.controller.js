const {
  getDetailPengajuanSeminar,
  updatePengajuanSeminarSidang,
} = require("../services/pengajuanSeminarSidang.service");
const { v4: uuidv4 } = require("uuid");
const {
  createSeminarSidang,
  getSeminarSidang,
  updateSeminarSidang,
  getAllSeminar,
  getDetailSeminar,
  getSeminarPenilaian,
  getSeminarPerbaikan,
  getSeminarSidangSuratTugas,
} = require("../services/seminarSidang.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const { createDok } = require("../services/dokumen.service");
const { updateStatus } = require("../services/aksesPengguna.service");
const { updatePengajuan } = require("../services/suratTugasDospem.service");
const pathDocument = process.env.BASE_URL + "documents/";
const dokumen = {
  pengajuanDospem: 1,
  suratTugasDospem: 2,
  pengajuanPerpanjanganDospem: 3,
  suratPerpanjanganTugasDospem: 4,
  proposalSkripsi: 5,
  suratBimbingan: 6,
  suratPernyataanDospem: 7,
  jadwalSempro: 8,
  revisiProposal: 9,
  beritaAcaraSempro: 10,
  penilaianSempro: 11,
  penilaianDospem1: 47,
  penilaianDospem2: 48,
  penilaianPenguji1: 50,
  penilaianPenguji2: 51,
};

class SeminarController {
  static async add(req, res, next) {
    try {
      const pengajuanSeminar = await getDetailPengajuanSeminar({
        id: req.body.pengajuanSeminarSidangId,
      });
      if (!pengajuanSeminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data pengajuan seminar tidak ditemukan",
        };
      }

      const seminar = await createSeminarSidang({
        uuid: uuidv4(),
        waktu: new Date(req.body.waktu),
        tempat: req.body.tempat,
        penguji1: req.body.penguji1,
        penguji2: req.body.penguji2,
      });

      await updatePengajuanSeminarSidang(
        {
          seminarSidangId: seminar.id,
        },
        {
          id: pengajuanSeminar.id,
        }
      );

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.jadwalSempro,
          seminarSidangId: seminar.id,
          suratTugasDospemId: pengajuanSeminar.suratTugasDospemId,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan jadwal seminar proposal"
      );
    } catch (error) {
      next(error);
    }
  }

  static async update(req, res, next) {
    try {
      const seminar = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        deletedAt: null,
        keterlaksanaan: null,
      });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data jadwal seminar proposal tidak ditemukan",
        };
      }

      let waktu = seminar.waktu;
      if (req.body.waktu) {
        waktu = new Date(req.body.waktu);
      }

      await updateSeminarSidang(
        {
          waktu,
          tempat: req.body.tempat || seminar.tempat,
          penguji1: req.body.penguji1 || seminar.penguji1,
          penguji2: req.body.penguji2 || seminar.penguji2,
        },
        {
          id: seminar.id,
        }
      );

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.jadwalSempro,
          seminarSidangId: seminar.id,
          suratTugasDospemId:
            seminar.PengajuanSeminarSidang.SuratTugasDospem.id,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil mengubah jadwal seminar"
      );
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const jadwalSeminar = await getSeminarSidang({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!jadwalSeminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data jadwal seminar proposal tidak ditemukan",
        };
      }

      await updateSeminarSidang(
        {
          deletedAt: new Date(),
        },
        {
          id: jadwalSeminar.id,
        }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Jadwal seminar proposal berhasil dihapus"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getAll(req, res, next) {
    try {
      const daftarJadwalSeminar = await getAllSeminar({
        keterlaksanaan: null,
        deletedAt: null,
      });
      if (daftarJadwalSeminar.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar jadwal seminar sedang kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar jadwal seminar ditemukan",
        daftarJadwalSeminar
      );
    } catch (error) {
      next(error);
    }
  }

  static async getDetail(req, res, next) {
    try {
      const detailSeminar = await getDetailSeminar({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!detailSeminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data jadwal seminar tidak ada",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar jadwal seminar ditemukan",
        detailSeminar
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadBeritaAcara(req, res, next) {
    try {
      const seminar = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: null,
        deletedAt: null,
      });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data seminar proposal tidak ditemukan",
        };
      }

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.beritaAcaraSempro,
        suratTugasDospemId: seminar.PengajuanSeminarSidang.SuratTugasDospem.id,
        seminarSidangId: seminar.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      await updateSeminarSidang(
        {
          keterlaksanaan: true,
        },
        {
          id: seminar.id,
        }
      );

      await updateStatus(
        5,
        seminar.PengajuanSeminarSidang.SuratTugasDospem.mahasiswaId
      );

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah berita acara seminar proposal"
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadPerbaikan(req, res, next) {
    try {
      const seminar = await getSeminarSidang({ uuid: req.params.uuid });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data seminar proposal tidak ditemukan",
        };
      }

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.revisiProposal,
        seminarSidangId: seminar.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah revisi proposal"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getPerbaikan(req, res, next) {
    try {
      const seminar = await getSeminarPerbaikan({ uuid: req.params.uuid });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data perbaikan proposal tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Data perbaikan seminar proposal ditemukan",
        seminar
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadPenilaian(req, res, next) {
    try {
      let seminar = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: true,
        deletedAt: null,
      });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data seminar proposal tidak ditemukan",
        };
      }

      let jenisDokumenId = null;
      let formNilai = {};

      switch (true) {
        case req.user.id == seminar.penguji1:
          jenisDokumenId = dokumen.penilaianPenguji1;
          formNilai.nilaiPenguji1 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id == seminar.penguji2:
          jenisDokumenId = dokumen.penilaianPenguji2;
          formNilai.nilaiPenguji2 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id ==
          seminar.PengajuanSeminarSidang.SuratTugasDospem.pembimbing1:
          jenisDokumenId = dokumen.penilaianDospem1;
          formNilai.nilaiDospem1 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id ==
          seminar.PengajuanSeminarSidang.SuratTugasDospem.pembimbing2:
          jenisDokumenId = dokumen.penilaianDospem2;
          formNilai.nilaiDospem2 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        default:
          break;
      }

      if (!jenisDokumenId) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak mengupload penilian seminar proposal ini",
        };
      }

      await updateSeminarSidang(formNilai, { id: seminar.id });

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId,
        seminarSidangId: seminar.id,
        suratTugasDospemId: seminar.PengajuanSeminarSidang.SuratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah hasil penilaian proposal"
      );

      seminar = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: true,
        deletedAt: null,
      });

      let nilaiAkhir =
        (seminar.nilaiDospem1 +
          seminar.nilaiDospem2 +
          seminar.nilaiPenguji1 +
          seminar.nilaiPenguji2) /
        4;

      await updatePengajuan(
        {
          nilaiSeminar: parseFloat(nilaiAkhir).toPrecision(3),
        },
        { id: seminar.PengajuanSeminarSidang.SuratTugasDospem.id }
      );
    } catch (error) {
      next(error);
    }
  }

  static async getPenilaian(req, res, next) {
    try {
      const seminar = await getSeminarPenilaian({ uuid: req.params.uuid });
      if (!seminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data penilaian seminar proposal tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Data penilaian seminar proposal ditemukan",
        seminar
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = SeminarController;
