const { v4: uuidv4 } = require("uuid");
const { getDetailSuratTugas } = require("../services/suratTugasDospem.service");
const {
  createPengajuanSeminar,
  getAllPengajuanSeminar,
  getDetailPengajuanSeminar,
} = require("../services/pengajuanSeminarSidang.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const dokumen = {
  pengajuanDospem: 1,
  suratTugasDospem: 2,
  pengajuanPerpanjanganDospem: 3,
  suratPerpanjanganTugasDospem: 4,
  proposalSkripsi: 5,
  suratBimbingan: 6,
  suratPernyataanDospem: 7,
};
const pathDocument = process.env.BASE_URL + "documents/";

class PengajuanSeminarController {
  static async pengajuanSeminar(req, res, next) {
    try {
      const suratTugasDospem = await getDetailSuratTugas({
        mahasiswaId: req.user.id,
        persetujuan: true,
      });
      if (!suratTugasDospem) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Surat Tugas Dospem tidak ditemukan",
        };
      }

      const pengajuanSeminar = await createPengajuanSeminar(
        {
          uuid: uuidv4(),
          suratTugasDospemId: suratTugasDospem.id,
          seminar: true,
          sidang: false,
        },
        {
          uuid: uuidv4(),
          jenisDokumenId: dokumen.proposalSkripsi,
          suratTugasDospemId: suratTugasDospem.id,
          filename: req.files.proposal_skripsi[0].filename,
          filesize: req.files.proposal_skripsi[0].size,
          mimetype: req.files.proposal_skripsi[0].mimetype,
          path: pathDocument + req.files.proposal_skripsi[0].filename,
        },
        {
          uuid: uuidv4(),
          jenisDokumenId: dokumen.suratBimbingan,
          suratTugasDospemId: suratTugasDospem.id,
          filename: req.files.surat_bimbingan[0].filename,
          filesize: req.files.surat_bimbingan[0].size,
          mimetype: req.files.surat_bimbingan[0].mimetype,
          path: pathDocument + req.files.surat_bimbingan[0].filename,
        },
        {
          uuid: uuidv4(),
          jenisDokumenId: dokumen.suratPernyataanDospem,
          suratTugasDospemId: suratTugasDospem.id,
          filename: req.files.surat_pernyataan_dospem[0].filename,
          filesize: req.files.surat_pernyataan_dospem[0].size,
          mimetype: req.files.surat_pernyataan_dospem[0].mimetype,
          path: pathDocument + req.files.surat_pernyataan_dospem[0].filename,
        }
      );

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengajukan seminar proposal"
      );
    } catch (error) {
      next(error);
    }
  }
  static async daftarPengajuanSeminar(req, res, next) {
    try {
      const daftarPengajuanSeminar = await getAllPengajuanSeminar();
      if (daftarPengajuanSeminar.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar Pengajuan Seminar Kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar pengajuan seminar ditemukan",
        daftarPengajuanSeminar
      );
    } catch (error) {
      next(error);
    }
  }
  static async detailPengajuanSeminar(req, res, next) {
    try {
      const detailPengajuanSeminar = await getDetailPengajuanSeminar({
        uuid: req.params.uuid,
      });
      if (!detailPengajuanSeminar) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data pengajuan seminar proposal tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data pengajuan seminar ditemukan",
        detailPengajuanSeminar
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = PengajuanSeminarController;
