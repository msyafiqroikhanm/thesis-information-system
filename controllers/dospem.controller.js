const {
  createPengajuan,
  getAllPengajuanDospem,
  getDetailPengajuanDospemByUuid,
  updatePengajuan,
  getAllPengajuaPerpanjanganDospem,
  getDetailPerpanjanganByUuid,
} = require("../services/suratTugasDospem.service");
const { createDok } = require("../services/dokumen.service");
const { v4: uuidv4 } = require("uuid");
const dokumen = {
  pengajuanDospem: 1,
  suratTugasDospem: 2,
  pengajuanPerpanjanganDospem: 3,
  suratPerpanjanganTugasDospem: 4,
};
require("dotenv").config();
const pathDocument = process.env.BASE_URL + "documents/";
const responseFormatter = require("../helpers/responseFormatter.helper");
const { updateStatus } = require("../services/aksesPengguna.service");

class DospemController {
  static async pengajuan(req, res, next) {
    try {
      if (!req.file) {
        throw {
          code: 400,
          status: "Bad Request",
          message: "Dokumen pengajuan dosen pembimbing wajib diisi",
        };
      }

      const pengajuan = await createPengajuan(
        {
          uuid: uuidv4(),
          mahasiswaId: req.user.id,
          pembimbing1: req.body.pembimbing1,
          pembimbing2: req.body.pembimbing2,
          judul: req.body.judul,
          deskripsi: req.body.deskripsi,
          keterangan: req.body.keterangan,
        },
        {
          uuid: uuidv4(),
          jenisDokumenId: dokumen.pengajuanDospem,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        }
      );

      await updateStatus(2, req.user.id);
      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengajukan penetapan dosen pembimbing"
      );
    } catch (error) {
      next(error);
    }
  }

  static async daftarPengajuan(req, res, next) {
    try {
      const daftarPengajuan = await getAllPengajuanDospem();

      let belumDisetujui = [];
      daftarPengajuan.forEach((element) => {
        let suratTugas = false;
        element.Dokumen.forEach((dok) => {
          if (dok.jenisDokumenId == 2) {
            suratTugas = true;
          }
          console.log(dok.jenisDokumenId);
        });
        if (!suratTugas) {
          belumDisetujui.push(element);
        }
      });

      if (daftarPengajuan.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar pengajuan penetapan dosen pembimbing kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar pengajuan penetapan dosen pembimbing ditemukan",
        belumDisetujui
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailPengajuan(req, res, next) {
    try {
      const detailPengajuan = await getDetailPengajuanDospemByUuid(
        req.params.uuid
      );

      const checkAkses = req.user.akses.find((e) => {
        e = 4;
      });

      if (checkAkses) {
        if (req.user.id !== detailPengajuan.Mahasiswa.id) {
          throw {
            code: 401,
            status: "Unauthorized Request",
            message: "Data tidak bisa diakses karena anda bukan pemilik",
          };
        }
      }

      if (!detailPengajuan) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data pengajuan tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data pengajuan ditemukan",
        detailPengajuan
      );
    } catch (error) {
      next(error);
    }
  }

  static async penetapan(req, res, next) {
    try {
      const detailPengajuan = await getDetailPengajuanDospemByUuid(
        req.params.uuid
      );

      if (!detailPengajuan) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data Pengajuan tidak ditemukan",
        };
      }

      const date = new Date();
      let formUpdate = {
        persetujuan: true,
        expiredAt: new Date(date.setMonth(date.getMonth() + 3)),
      };
      if (req.body.pembimbing1 && req.body.pembimbing2) {
        formUpdate.pembimbing1 = req.body.pembimbing1;
        formUpdate.pembimbing2 = req.body.pembimbing2;
      }

      const penetapan = await updatePengajuan(formUpdate, {
        id: detailPengajuan.id,
      });

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.suratTugasDospem,
          suratTugasDospemId: penetapan[1][0].id,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      await updateStatus(3, mahasiswaId);

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Pengajuan berhasil disetujui"
      );
    } catch (error) {
      next(error);
    }
  }

  static async pengajuanPerpanjangan(req, res, next) {
    try {
      const suratTugasDospem = await getDetailPengajuanDospemByUuid(
        req.params.uuid
      );
      if (!suratTugasDospem) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Surat Tugas Dosen Pembimbing tidak ditemukan",
        };
      }

      const date = new Date();
      const perpanjanganSuratTugasDospem = await updatePengajuan(
        {
          persetujuan: null,
        },
        { id: suratTugasDospem.id }
      );

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.pengajuanPerpanjanganDospem,
        suratTugasDospemId: suratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil mengajukan perpanjangan penetapan dosen pembimbing"
      );
    } catch (error) {
      next(error);
    }
  }

  static async daftarPengajuanPerpanjangan(req, res, next) {
    try {
      const daftarPengajuanPerpanjangan =
        await getAllPengajuaPerpanjanganDospem();

      if (!daftarPengajuanPerpanjangan) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Daftar pengajuan perpanjangan penetapan dosen pembimbing kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar pengajuan perpanjangan penetapan dosen pembimbing ditemukan",
        daftarPengajuanPerpanjangan
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailPengajuanPerpanjangan(req, res, next) {
    try {
      const detailPerpanjangan = await getDetailPerpanjanganByUuid(
        req.params.uuid
      );
      if (!detailPerpanjangan) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Data perpanjangan penetapan dosen pembimbing tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data perpanjangan penetapan dosen pembimbing ditemukan",
        detailPerpanjangan
      );
    } catch (error) {
      next(error);
    }
  }

  static async penetapanPerpanjangan(req, res, next) {
    try {
      const detailPerpanjangan = await getDetailPerpanjanganByUuid(
        req.params.uuid
      );
      if (!detailPerpanjangan) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Data perpanjangan penetapan dosen pembimbing tidak ditemukan",
        };
      }

      const date = new Date();
      await updatePengajuan(
        {
          persetujuan: true,
          expiredAt: new Date(date.setMonth(date.getMonth() + 3)),
        },
        { id: detailPerpanjangan.id }
      );

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.suratPerpanjanganTugasDospem,
          suratTugasDospemId: detailPerpanjangan.id,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menetapkan perpanjangan dosen pembimbing"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = DospemController;
