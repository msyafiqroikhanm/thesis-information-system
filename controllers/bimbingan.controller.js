require("dotenv").config();
const {
  createBimbingan,
  getBimbinganBy,
  softDeleteBimbingan,
  getAllBimbingan,
} = require("../services/bimbingan.service");
const { v4: uuidv4 } = require("uuid");
const { getSuratTugasBy } = require("../services/suratTugasDospem.service");
const responseFormatter = require("../helpers/responseFormatter.helper");

class BimbinganController {
  static async createBimbingan(req, res, next) {
    try {
      if (!req.file) {
        throw {
          code: 400,
          status: "Bad Request",
          message: "Dokumentasi wajib diisi",
        };
      }

      const suratTugasDospem = await getSuratTugasBy({
        mahasiswaId: req.user.id,
        persetujuan: true,
      });

      if (!suratTugasDospem) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Surat Tugas Dospem tidak ditemukan",
        };
      }

      const bimbingan = await createBimbingan({
        uuid: uuidv4(),
        suratTugasDospemId: suratTugasDospem.id,
        topik: req.body.topik,
        catatan: req.body.catatan,
        waktu: new Date(req.body.waktu),
        tempat: req.body.tempat,
        dokumentasi: process.env.BASE_URL + "documents/" + req.file.filename,
      });

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan riwayat bimbingan"
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteBimbingan(req, res, next) {
    try {
      const bimbingan = await getBimbinganBy({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!bimbingan) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data bimbingan tidak ditemukan",
        };
      }

      if (bimbingan.SuratTugasDospem.mahasiswaId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda bukan pemilik data",
        };
      }

      await softDeleteBimbingan({ id: bimbingan.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data bimbingan berhasil di hapus"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getAllBimbingan(req, res, next) {
    try {
      const daftarBimbingan = await getAllBimbingan();
      if (daftarBimbingan.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data bimbingan tidak ditemukan",
        };
      }

      let resultDataByAkses = [];
      req.user.akses.forEach((element) => {
        console.log(element);
        if (element == 2) {
          daftarBimbingan.forEach((element) => {
            if (element.SuratTugasDospem.pembimbing1 == req.user.id) {
              resultDataByAkses.push(element);
            } else if (element.SuratTugasDospem.pembimbing2 == req.user.id) {
              resultDataByAkses.push(element);
            }
          });
        } else if (element == 4) {
          daftarBimbingan.forEach((element) => {
            if (element.SuratTugasDospem.mahasiswaId == req.user.id) {
              resultDataByAkses.push(element);
            }
          });
        } else {
          if (element == 3) {
            resultDataByAkses = daftarBimbingan;
          }
        }
      });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data bimbingan ditemukan",
        resultDataByAkses
      );
    } catch (error) {
      next(error);
    }
  }

  static async getDetailBimbingan(req, res, next) {
    try {
      const bimbingan = await getBimbinganBy({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!bimbingan) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data bimbingan tidak ditemukan",
        };
      }

      let dataBimbinganByAkses;
      req.user.akses.forEach((element) => {
        console.log(element);
        if (element == 2) {
          if (
            bimbingan.SuratTugasDospem.pembimbing1 == req.user.id ||
            bimbingan.SuratTugasDospem.pembimbing2 == req.user.id
          ) {
            dataBimbinganByAkses = bimbingan;
          }
        } else if (element == 4) {
          if (bimbingan.SuratTugasDospem.mahasiswaId == req.user.id) {
            dataBimbinganByAkses = bimbingan;
          }
        } else {
          if (element == 3) {
            resultDataByAkses = bimbingan;
          }
        }
      });

      if (!dataBimbinganByAkses) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda tidak memiliki akses ke data",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data bimbingan ditemukan",
        dataBimbinganByAkses
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = BimbinganController;
