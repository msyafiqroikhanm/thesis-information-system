const {
  getUserByEmail,
  createUser,
  getAllUsers,
  getAllInfoUsers,
  getUserByUuid,
  getUserActiveByUuid,
  softDeleteUser,
  getUserBioBy,
} = require("../services/pengguna.service");
const { updateBio } = require("../services/biodataPengguna.service");
const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");
const responseFormatter = require("../helpers/responseFormatter.helper");

class UserController {
  static async add(req, res, next) {
    try {
      let formBio;
      const user = await getUserByEmail(req.body.email);

      if (user) {
        throw {
          code: 409,
          status: "Conflict",
          message: "Email sudah terdaftar",
        };
      }

      const salt = bcrypt.genSaltSync(10);
      const password = bcrypt.hashSync(
        req.body.nim || req.body.nidn || req.body.nip,
        salt
      );

      let formUser = {
        email: req.body.email,
        password,
        uuid: uuidv4(),
        active: true,
      };

      let formAkses = {
        aksesId: req.body.akses,
      };

      if (req.body.nim) {
        formAkses.statusId = 1;
        formBio = {
          nama: req.body.nama,
          nim: req.body.nim,
          angkatan: req.body.angkatan,
        };
      }

      if (req.body.nidn) {
        formBio = {
          nama: req.body.nama,
          nidn: req.body.nidn,
          nip: req.body.nip,
        };
      }

      const newUser = await createUser(formUser, formBio, formAkses);
      return responseFormatter.success(
        res,
        201,
        "Created",
        "Pengguna berhasil ditambahkan"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getAll(req, res, next) {
    try {
      const users = await getAllUsers();
      if (users.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Tidak ada pengguna",
        };
      }
      return responseFormatter.success(res, 200, "OK", "Data Ditemukan", users);
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const user = await getUserActiveByUuid(req.params.uuid);
      if (!user) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Pengguna tidak terdaftar",
        };
      }

      await softDeleteUser({ id: user.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Pengguna berhasil dihapus"
      );
    } catch (error) {
      next(error);
    }
  }

  static async showBiodata(req, res, next) {
    try {
      const userBio = await getUserBioBy({
        id: req.user.id,
        active: true,
        deletedAt: null,
      });

      if (!userBio) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Biodata tidak ditemukan",
        };
      }

      if (userBio.email !== req.user.email) {
        throw {
          code: 406,
          status: "Not Acceptable",
          message: "Tidak diizinkan",
        };
      }
      return responseFormatter.success(
        res,
        200,
        "OK",
        "Biodata berhasil ditemukan",
        userBio
      );
    } catch (error) {
      next(error);
    }
  }

  static async updateUserBiodata(req, res, next) {
    try {
      const userBio = await getUserBioBy({
        uuid: req.params.uuid,
        active: true,
        deletedAt: null,
      });

      if (!userBio) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Biodata tidak ditemukan",
        };
      }

      if (userBio.email !== req.user.email) {
        throw {
          code: 406,
          status: "Not Acceptable",
          message: "Tidak diizinkan",
        };
      }

      await updateBio(
        {
          nip: req.body.nip,
          nidn: req.body.nidn,
          nim: req.body.nim,
          angkatan: req.body.angkatan,
        },
        { id: userBio.Biodata.id }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Biodata berhasil diperbarui",
        userBio
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserController;
