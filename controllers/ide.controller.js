const ResponseFormatter = require("../helpers/responseFormatter.helper");
const {
  insertIdePenelitian,
  selectAllIdeas,
  selectOneIdea,
  softDeleteIdea,
} = require("../services/ide.service");
const { v4: uuidv4 } = require("uuid");
const {
  insertPengajuanPenelitianDosen,
  selectAllPengajuanPenelitian,
  selectOnePengajuanPenelitian,
  updatePengajuanPenelitian,
} = require("../services/pengajuanPenelitianIdeDosen.service");
const pathDocument = process.env.BASE_URL + "documents/";

class IdeController {
  static async createIde(req, res, next) {
    try {
      req.body.uuid = uuidv4();
      req.body.dosenId = req.user.id;
      await insertIdePenelitian(req.body);

      return ResponseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan ide penelitian"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getAllIde(req, res, next) {
    try {
      const ideas = await selectAllIdeas();
      if (ideas.length == 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Ide penelitian sedang kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Daftar ide penelitian ditemukan",
        ideas
      );
    } catch (error) {
      next(error);
    }
  }

  static async getDetailIde(req, res, next) {
    try {
      const ide = await selectOneIdea({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!ide) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Detail ide penelitian ditemukan",
        ide
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteIde(req, res, next) {
    try {
      const ide = await selectOneIdea({
        uuid: req.params.uuid,
        pelaksana: null,
        deletedAt: null,
      });
      if (!ide) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      if (ide.dosenId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak menghapus ide penelitian ini",
        };
      }

      await softDeleteIdea({ id: ide.id });

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menghapus ide penelitian"
      );
    } catch (error) {
      next(error);
    }
  }

  static async createPengajuanPenelitian(req, res, next) {
    try {
      const idePenelitian = await selectOneIdea({
        uuid: req.params.uuid,
        pelaksana: null,
        deletedAt: null,
      });
      if (!idePenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      // res.send(req.files.biodata[0]s.biodata[0]);

      await insertPengajuanPenelitianDosen(
        {
          uuid: uuidv4(),
          mahasiswaId: req.user.id,
          idePenelitianId: idePenelitian.id,
        },
        {
          uuid: uuidv4(),
          jenisDokumenId: 12, //biodata
          filename: req.files.biodata[0].filename,
          filesize: req.files.biodata[0].size,
          mimetype: req.files.biodata[0].mimetype,
          path: pathDocument + req.files.biodata[0].filename,
        }
      );

      return ResponseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengajukan penelitian ide dosen"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getPengajuanPenelitian(req, res, next) {
    try {
      const idePenelitian = await selectOneIdea({
        uuid: req.params.uuid,
        pelaksana: null,
        deletedAt: null,
      });

      if (!idePenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      const daftarPengajuanPenelitian = await selectAllPengajuanPenelitian({
        idePenelitianId: idePenelitian.id,
      });

      if (daftarPengajuanPenelitian.length == 0) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Daftar mahasiswa yang mengajukan melakukan penelitian masih kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Daftar mahasiswa yang mengajukan melakukan penelitian ditemukan",
        { idePenelitian, daftarPengajuanPenelitian }
      );
    } catch (error) {
      next(error);
    }
  }

  static async acceptPengajuanPenelitian(req, res, next) {
    try {
      const idePenelitian = await selectOneIdea({
        uuid: req.params.uuid_ide,
        pelaksana: null,
        deletedAt: null,
      });

      if (!idePenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      if (idePenelitian.dosenId != req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak atas data ini",
        };
      }

      const dataPengajuanPenelitian = await selectOnePengajuanPenelitian({
        uuid: req.params.uuid_penelitian,
      });

      if (!dataPengajuanPenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data Pengajuan Penelitian Ide Dosen tidak ditemukan",
        };
      }

      await updatePengajuanPenelitian(
        { penerimaan: true },
        { id: dataPengajuanPenelitian.id }
      );

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menyetujui pengajuan penelitian ide dosen"
      );
    } catch (error) {
      next(error);
    }
  }

  static async rejectPengajuanPenelitian(req, res, next) {
    try {
      const idePenelitian = await selectOneIdea({
        uuid: req.params.uuid_ide,
        pelaksana: null,
        deletedAt: null,
      });

      if (!idePenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail ide penelitian tidak ditemukan",
        };
      }

      if (idePenelitian.dosenId != req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak atas data ini",
        };
      }

      const dataPengajuanPenelitian = await selectOnePengajuanPenelitian({
        uuid: req.params.uuid_penelitian,
      });

      if (!dataPengajuanPenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data Pengajuan Penelitian Ide Dosen tidak ditemukan",
        };
      }

      await updatePengajuanPenelitian(
        { penerimaan: false, keterangan: req.body.keterangan },
        { id: dataPengajuanPenelitian.id }
      );

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menolak pengajuan penelitian ide dosen"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = IdeController;
