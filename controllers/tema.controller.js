const ResponseFormatter = require("../helpers/responseFormatter.helper");
const {
  getUserBioBy,
  getUserBioTema,
} = require("../services/pengguna.service");
const {
  insertTema,
  selectOneTema,
  softDeleteTema,
} = require("../services/tema.service");
const { v4: uuidv4 } = require("uuid");

class TemaController {
  static async addTema(req, res, next) {
    try {
      const user = await getUserBioBy({ id: req.user.id });

      await insertTema({
        uuid: uuidv4(),
        biodataId: user.Biodata.id,
        nama: req.body.nama,
      });

      return ResponseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan minat tema penelitian"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getTema(req, res, next) {
    try {
      const userInfo = await getUserBioTema({ uuid: req.params.uuid });
      if (!userInfo) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data pengguna tidak ditemukan",
        };
      }

      if (userInfo.Biodata.Minat.length == 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data minat pengguna kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Data tema pengguna ditemukan",
        userInfo
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteTema(req, res, next) {
    try {
      const tema = await selectOneTema({
        uuid: req.params.uuid,
        deletedAt: null,
      });

      if (!tema) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data tema penelitian pengguna tidak ditemukan",
        };
      }

      const user = await getUserBioBy({ id: req.user.id });

      if (tema.biodataId !== user.Biodata.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak menghapus tema penelitian ini",
        };
      }

      await softDeleteTema({ deletedAt: new Date() }, { id: tema.id });

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menghapus tema penelitian"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = TemaController;
